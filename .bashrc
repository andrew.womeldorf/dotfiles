export BROWSER=/usr/bin/firefox
export EDITOR=/usr/local/bin/vim
export PS1="$ "

alias config='git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias ls='ls --color'
alias rg='rg $RG_FLAGS'

# Change directory to the current git root
...() {
    cd $(git rev-parse --show-toplevel)
}

# Open the current git repo in the browser
gopen() {
    url="$(git remote get-url --push origin | perl -pe 's|:(?!/)|/|' | perl -pe 's|git@|https://|' | perl -pe 's|\.git$||')"
    if [[ "$(uname)" == "Darwin" ]]; then
        open "$url"
    else
        firefox "$url"
    fi
}

# Please
# I put the completion script with other bash completions
if command -v please &> /dev/null; then
    export PATH="${PATH}:${HOME}/.please:${HOME}/bin"
    source <(please --completion_script)
fi

# FZF
if [ -f $HOME/.fzf.bash ]; then
    export FZF_COMPLETION_TRIGGER="~~"
    export FZF_DEFAULT_COMMAND="rg --files --hidden --follow --ignore"
    #source /usr/share/doc/fzf/examples/completion.bash
    source "$HOME/.fzf.bash"
fi

# FLY.IO
if [ -d "$HOME/.fly" ]; then
    export FLYCTL_INSTALL="$HOME/.fly"
    export PATH="$FLYCTL_INSTALL/bin:$PATH"
fi

# POETRY
export PATH="$HOME/.local/bin:$PATH"

# RESTIC
export RESTIC_REPOSITORY_FILE="$HOME/.config/restic/repository"
export RESTIC_PASSWORD_FILE="$HOME/.config/restic/password"

# RUST
if [ -e "$HOME/.cargo/env" ]; then
    . "$HOME/.cargo/env"
fi

# ALACRITTY
if [ -f "$HOME/Work/src/github.com/alacritty/alacritty/extra/completions/alacritty.bash" ]; then
    export TERMINAL=alacritty
    source "$HOME/Work/src/github.com/alacritty/alacritty/extra/completions/alacritty.bash"
fi

# AWS
if [ -f '/usr/local/bin/aws_completer' ]; then
    complete -C '/usr/local/bin/aws_completer' aws
fi

# SECRETS
if [ -e $HOME/.secrets ]; then
    . $HOME/.secrets
fi

# GO
export GOPATH="${HOME}/Work"
export GOPRIVATE=gitlab.com
export PATH="${GOPATH}/bin:/usr/local/go/bin:${PATH}"

# MISE/RTX (ASDF)
eval "$(${HOME}/.local/bin/mise activate bash)"
export PATH="${PATH}:/home/andrew/.local/share/mise/installs/cargo-trippy/0.12.0/bin"

# someone from work made this. use when you want to search for something in
# your codebase and preview the results.
# requires rg, fzf, bat
rgv() {
    rg --column $@ \
    | fzf \
        --preview "bat --color=always --style full --highlight-line {2} {1}" \
        --delimiter ":" \
        --preview-window '~4,+{2}/2,bottom:80%'
}
