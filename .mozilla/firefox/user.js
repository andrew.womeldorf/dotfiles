// Homepage and New Tab open to blank
user_pref("browser.startup.homepage", "about:blank");
user_pref("browser.newtabpage.enabled", false);

// Prefered Search Engine
user_pref("browser.urlbar.placeholderName", "DuckDuckGo");

// Search Shortcuts that I don't want
user_pref("browser.search.hiddenOneOffs", "Google,Amazon.com,Bing,DuckDuckGo,eBay,Wikipedia (en)");

// Privacy
// DNT
user_pref("privacy.donottrackheader.enabled", true);
// Autofill Passwords
user_pref("signon.rememberSignons", false);
// Autofill Addresses
user_pref("extensions.formautofill.addresses.enabled", false);
// Autofill Credit Cards
user_pref("extensions.formautofill.creditCards.enabled", false);
// Allow Firefox to install and run studies
user_pref("app.shield.optoutstudies.enabled", false);
// Allow Firefox to send technical and interaction data to Mozilla
user_pref("datareporting.healthreport.uploadEnabled", false);
// Allow Firefox to install and run studies
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
