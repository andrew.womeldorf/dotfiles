#!/bin/sh
#
# This is the script that backs up the homedir with restic via cron.
# When restoring to a new hard drive, recreate the cron:
#
#   0 1 * * * /home/andrew/.backup.sh 2>&1 | /usr/bin/logger --tag RESTIC
#
# Output of the script goes to /var/log/syslog
#
# The wasabi access credentials must be set environment variables.
#
#   export AWS_ACCESS_KEY_ID=
#   export AWS_SECRET_ACESS_KEY=

. /home/andrew/.config/restic/environment

restic backup \
    --tag cron \
    --repository-file /home/andrew/.config/restic/repository \
    --password-file /home/andrew/.config/restic/password \
    --exclude-file /home/andrew/.resticignore \
    /home/andrew

    #--exclude .cache/ \
