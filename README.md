# User Config

[Follows this setup](https://www.atlassian.com/git/tutorials/dotfiles).

The repo name in Gitlab is dotfiles because I already had a `cfg`. The alias in
`.bashrc` is to `~/.cfg`. So...clone appropriately, or change the alais for
`config()`.

## Firefox

I'm new to Firefox config, so this is a little more janky than others.

[User.js](http://kb.mozillazine.org/User.js_file) allows setting preferences
from a file. Any modifications made in the UI are supposedly overwritten by the
contents of this file. It needs to go in a profile, but I don't know if/how to
guarantee a profile name, so I put the file generically in `.mozilla/user.js`.
You should create a symlink into the active profile:

```bash
ln -s $HOME/.mozilla/<PROFILE>/user.js $HOME/.mozilla/user.js
```
