vim.g.mapleader = ","

-- Colors
vim.cmd.colorscheme('neodark')

-- Options
--
-- Use the system clipboard
vim.opt.clipboard = "unnamed"

-- Search, case-insensitive, except when search
-- contains capital letters, then case sensitive.
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Show line numbers.
vim.opt.number = true

-- Where to store swapfiles.
vim.opt.directory = vim.fn.expand("$HOME/.config/nvim/swapfiles/")

-- Show X lines before/after cursor position.
vim.opt.scrolloff = 3

-- Detect file changes from outside of vim and reload.
vim.opt.autoread = true

-- Show matching brackets.
vim.opt.showmatch = true

-- Tenths of a second to show matching brackets.
vim.opt.matchtime = 2

-- Do not keep search results highlighted.
vim.opt.hlsearch = false

-- Always show the status line
vim.opt.laststatus = 2
vim.opt.statusline = "%f Line: %l/%L  Column: %c"

vim.opt.textwidth = 89

-- Tabbing...I don't understand the docs. Work in progress.
-- :h tabstop
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.api.nvim_create_autocmd("FileType", {
    pattern = {"html", "javascript", "json", "lua", "proto", "scss", "svelte", "typescript", "yaml"},
    callback = function(args)
        vim.opt_local.tabstop = 2
        vim.opt_local.softtabstop = 2
        vim.opt_local.shiftwidth = 2
    end
})
vim.api.nvim_create_autocmd("FileType", {
    pattern = {"make"},
    callback = function(args)
        vim.opt_local.shiftwidth = 8
    end
})

-- Mapping
vim.keymap.set('n', '<Up>', ':resize +2<CR>')
vim.keymap.set('n', '<Down>', ':resize -2<CR>')
vim.keymap.set('n', '<Left>', ':vertical resize +2<CR>')
vim.keymap.set('n', '<Right>', ':vertical resize -2<CR>')

-- Plugins
--
-- CtrlP
vim.g.ctrlp_user_command = {'.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard'}
vim.g.ctrlp_map = '<c-p>'
vim.g.ctrlp_cmd = 'CtrlP'
vim.g.ctrlp_custom_ignore = {
  dir = '\\v[\\/](\\.(git|hg|svn)|node_modules|vendor|docs)$',
  file = '\\v\\.(exe|so|dll|class|png|jpg|jpeg)$',
}

-- Tagbar
vim.keymap.set('n', '<leader>t', ':TagbarToggle<CR>')

-- vim-terraform
vim.g.terraform_align = 1
vim.g.terraform_fmt_on_save = 1

require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all" (the listed parsers MUST always be installed)
  ensure_installed = {
    "c",
    "lua",
    "markdown",
    "markdown_inline",
    "query",
    "vim",
    "vimdoc",
  },


  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = true,

  ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
  -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

  highlight = {
    enable = true,

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}
